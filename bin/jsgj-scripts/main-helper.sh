#!/bin/bash

# set -e

source $OPENSHIFT_CARTRIDGE_SDK_BASH

source ${OPENSHIFT_JSGJ_DIR}/bin/jsgj-scripts/util-functions.sh

source ${OPENSHIFT_JSGJ_DIR}/bin/jsgj-scripts/setting-java-opt-environment-variables.sh

source ${OPENSHIFT_JSGJ_DIR}/bin/jsgj-scripts/setting-VCAP_SERVICES.sh

