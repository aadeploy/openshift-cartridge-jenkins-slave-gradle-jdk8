#!/bin/bash 

# this file is sourced by main-helper.sh which in turn is sourced by all life-cycle scripts

# its purpose is to set environment variables that are used as command line parameters
# when the application (java) is started 

export OPENSHIFT_JSGJ_JAVA_OPTS_SERVER_NET=" -Dserver.address=${OPENSHIFT_JSGJ_IP} -Dserver.port=${OPENSHIFT_JSGJ_HTTP_PORT}"
if [[ -n "${OPENSHIFT_JSGJ_JAVA_OPTS_SERVER_NET_OVERRIDE}"  ]]; then
   OPENSHIFT_JSGJ_JAVA_OPTS_SERVER_NET=${OPENSHIFT_JSGJ_JAVA_OPTS_SERVER_NET_OVERRIDE}
fi


export OPENSHIFT_JSGJ_JAVA_OPTS=" -server -Dlogging.path=${OPENSHIFT_JSGJ_LOG_DIR} -Dspring.profiles.active=cloud"
if [[ -n "${OPENSHIFT_JSGJ_JAVA_OPTS_OVERRIDE}"  ]]; then
   OPENSHIFT_JSGJ_JAVA_OPTS=${OPENSHIFT_JSGJ_JAVA_OPTS_OVERRIDE}
fi
