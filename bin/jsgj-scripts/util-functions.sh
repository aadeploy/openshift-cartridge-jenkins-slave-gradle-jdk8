#!/bin/bash 

function update_configuration {
  setup-jdk-8
  setup-gradle-23
  export_java_home
  export_gradle_home
  reinstall_path
}

function export_java_home() {
  export JAVA_HOME="$OPENSHIFT_JSGJ_DIR/jdk/jdk1.8.0_40"
}

function export_gradle_home() {
  export GRADLE_HOME="$OPENSHIFT_JSGJ_DIR/gradle/gradle-2.4"
}

function reinstall_path {
  echo $JAVA_HOME > $OPENSHIFT_JSGJ_DIR/env/JAVA_HOME
  echo $GRADLE_HOME > $OPENSHIFT_JSGJ_DIR/env/GRADLE_HOME
  echo "$JAVA_HOME/bin:$GRADLE_HOME/bin" > $OPENSHIFT_JSGJ_DIR/env/OPENSHIFT_JSGJ_PATH_ELEMENT
}

function cart-log {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_JSGJ_LOG_DIR}/cart-JSGJ-log.out
}

function cart-log-message {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_JSGJ_LOG_DIR}/cart-JSGJ-log.out
  client_message  "$( date "+%Y%m%d_%H%M%S" ) $@ "
}


function cart-log-error {
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " >>  ${OPENSHIFT_JSGJ_LOG_DIR}/cart-JSGJ-log.out
  client_error "$( date "+%Y%m%d_%H%M%S" ) $@ "
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " 1>&2
}

function rotate_logs() {
   mv ${OPENSHIFT_JSGJ_LOG_DIR}/java.out ${OPENSHIFT_JSGJ_LOG_DIR}/java.out.$( date "+%Y%m%d_%H%M%S" )
   mv ${OPENSHIFT_JSGJ_LOG_DIR}/java.err.out ${OPENSHIFT_JSGJ_LOG_DIR}/java.err.out$( date "+%Y%m%d_%H%M%S" )
}

function setup-gradle-23() {
  echo "Downloading and Configuring Gradle 2.4 Nightly"
  if [ ! -d "${OPENSHIFT_JSGJ_DIR}/gradle" ]; then
    mkdir -p ${OPENSHIFT_JSGJ_DIR}/gradle
  fi

  if [ ! -d "${OPENSHIFT_JSGJ_DIR}/gradle/gradle-2.4" ]; then
    pushd ${OPENSHIFT_JSGJ_DIR}/gradle
    # https://services.gradle.org/versions/nightly
    wget -O gradle-2.4-bin.zip https://services.gradle.org/distributions-snapshots/gradle-2.4-20150330060505+0000-bin.zip
    unzip gradle-2.4-bin.zip
    rm gradle-2.4-bin.zip
    mv gradle-2.4-* gradle-2.4
  fi

  echo "Gradle 2.4 Nightly Downloaded"
}

function setup-jdk-8() {
  echo "Downloading and Configuring Java 8"
  if [ ! -d "${OPENSHIFT_JSGJ_DIR}/jdk" ]; then
    mkdir -p ${OPENSHIFT_JSGJ_DIR}/jdk
  fi
  
  if [ ! -d "${OPENSHIFT_JSGJ_DIR}/jdk/jdk1.8.0_40" ]; then
    pushd ${OPENSHIFT_JSGJ_DIR}/jdk
    wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u40-b26/jdk-8u40-linux-x64.tar.gz
    tar -zxf jdk-8u40-linux-x64.tar.gz
    rm jdk-8u40-linux-x64.tar.gz
  fi
  echo "Java 8 Downloaded"
}

